 [repo link](https://bitbucket.org/risovaxa/developex_test/)
## Tools:
 * Qt 5.11.0 MSVC2015 x86
 * WinSDK: 10.0.17763.0
 * qmake
 
### Tests:
 1. To run tests switch to "test_pro" project
 2. If you are using QtCreator run Tools > Tests > Run All Tests
 3. Else run
	* ```/test_pro$ qmake -project "QT += testlib" //requires full path to qmake ```
	* ```	/test_pro$ qmake```
	* ```	/test_pro$ nmake   //on this step don't forget to setup environment variables for Qt and Win```
	
	or replace last line with
	
	* ```	/test_pro$ jom.exe //it depends on preferable environment (VS or Qt)```

