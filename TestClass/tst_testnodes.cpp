#include <QtTest>
#include "../helper.h"

// add necessary includes here

class TestNodes : public QObject
{
    Q_OBJECT

public:
    TestNodes();
    ~TestNodes();

private slots:
    void testCreateNodes();

    // have nullptr leaf on root->left->right = createNode(10)
    void testLinkingForNoRightLeaf();

    void testLinkingForNoRoot();

    void testLinkingForNoRightRoot();
    void testLinkingForNoLeftRoot();

};

TestNodes::TestNodes()
{

}

TestNodes::~TestNodes()
{

}

void TestNodes::testCreateNodes()
{
    int value = 0;
    QVERIFY(createNode(value) != nullptr);

    value = 1000;
    QVERIFY(createNode(value) != nullptr);

    value = 2147483647; // INT_MAX
    QVERIFY(createNode(value) != nullptr);
}

void TestNodes::testLinkingForNoRightLeaf()
{
    node* root = createNode(1);
    root->left = createNode(5);
    root->right = createNode(7);
    root->right->right = createNode(3);
    root->right->left = createNode(4);
    root->left->left = createNode(2);
//    root->left->right = createNode(10);

    linkSameLevel(root);
    QCOMPARE(root->left->left->level, root->right->left);
}

void TestNodes::testLinkingForNoRoot()
{
    node* root = createNode(1);

    linkSameLevel(root);

    QCOMPARE(root->left, nullptr);
    QCOMPARE(root->right, nullptr);
}

void TestNodes::testLinkingForNoRightRoot()
{
    node* root = createNode(1);
    root->left = createNode(5);
    root->left->left = createNode(2);
//    root->left->right = createNode(10);

    linkSameLevel(root);
    QCOMPARE(root->left->left->level, nullptr);
    QCOMPARE(root->left->level, nullptr);
}

void TestNodes::testLinkingForNoLeftRoot()
{
    node* root = createNode(1);
    root->right = createNode(7);
    root->right->right = createNode(3);
    root->right->left = createNode(4);

    linkSameLevel(root);
    QCOMPARE(root->right->left->level, root->right->right);
}

QTEST_APPLESS_MAIN(TestNodes)

#include "tst_testnodes.moc"
