#include <QDebug>
#include <iostream>
#include <list>

#ifndef HELPER_H
#define HELPER_H

struct node
{
    int n;
    node* left;
    node* right;
    node* level;

};

node* createNode(int value)
{
    node* newNode = new node;
    newNode->n = value;
    newNode->right = nullptr;
    newNode->left = nullptr;
    newNode->level = nullptr;
    return newNode;
}

void linkSameLevel(node* node_)
{
    // algorithm point is pushing every node in list separating levels with
    // nullptr

    if(!node_)
        return;

    std::list<node*> level_list;
    level_list.push_back(node_);
    level_list.push_back(nullptr); //end of the level

    while(!level_list.empty()) {
        node* front_node = level_list.front();
        level_list.pop_front();

        if(front_node != nullptr) {
            front_node->level = level_list.front();

            if (front_node->left)
                level_list.push_back(front_node->left);
            if (front_node->right)
                level_list.push_back(front_node->right);
        }
        else if (!level_list.empty()){
//            qDebug() << "Level list: " << level_list;
            level_list.push_back(nullptr);
        }
    }
    qDebug() << "Algo end.";
}


#endif // HELPER_H
