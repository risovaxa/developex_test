// https://bitbucket.org/risovaxa/developex_test/

#include <QCoreApplication>
#include "helper.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    // for tree with n nodes we have time complexity O(n),
    // because we are visiting every node only once
    // d (depth) value doesn't matter
    // In worst case we are using memory for n * 2 nodes,
    // when we have only one node at level
    // so mem complexity is O(n)

    node* root = createNode(1);
    root->left = createNode(5);
    root->right = createNode(7);
    root->right->right = createNode(3);
    root->right->left = createNode(4);
    root->left->left = createNode(2);
    root->left->right = createNode(10);

    linkSameLevel(root);

    return a.exec();
}
